Shader "VesceTools/PreviewShader"
{
    Properties
    {
        _Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _Alpha ("Alpha", Range(0,1)) = 0.5 

        _AmbientLight("Ambient Light", Range(0,1)) = 0.2
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD0;
            };

            float4 _Color;
            float _Alpha;
            float _AmbientLight;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // Color
                float light = dot(i.normal, _WorldSpaceLightPos0.xyz);
                float shading = max(_AmbientLight, light);
                float3 col = _Color.xyz * shading;

                return float4(col, _Alpha);
            }
            ENDCG
        }
    }
}
