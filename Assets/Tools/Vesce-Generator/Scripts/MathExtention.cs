﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VesceGenerator
{
    public static class MathExtention
    {
        public static int ToClosestInt(this int x, int value)
        {
            int division = (int)x / (int)value;
            int smallBoundary = value * division;
            int bigBoundary = (value) * (division + 1);

            int smallDifference = x - smallBoundary;
            int bigDifference = bigBoundary - x;

            return bigDifference <= smallDifference ? bigBoundary : smallBoundary;
        }

        public static int To360Angle(this int x)
        {
            int angle = x % 360;

            if (angle < 0)
                angle += 360;

            if(angle == 360)
                angle = 0;

            return angle;
        }
    
        public static float AtLeast(this float a, float x)
        {
            return MathF.Max(a, x);
        }
    }
}
