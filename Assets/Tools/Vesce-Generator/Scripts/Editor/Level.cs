﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace VesceGenerator
{
    public class Level : ScriptableObject
    {
        [SerializeField] private SavedRoom[] savedRooms;

        private Preview[] previews;

        public void SaveData(List<Room> rooms)
        {
            savedRooms = new SavedRoom[rooms.Count];

            for (int i = 0; i < savedRooms.Length; i++)
            {
                savedRooms[i] = new SavedRoom(
                    PrefabUtility.GetCorrespondingObjectFromSource(rooms[i]).gameObject,
                    rooms[i].transform.localToWorldMatrix);
            }
        }

        public void GeneratePreviews(Material material)
        {
            previews = new Preview[savedRooms.Length];

            for(int i = 0; i < savedRooms.Length; i++)
            {
                previews[i] = new Preview(material, savedRooms[i].prefab);
            }
        }

        public void DrawPreview(Vector3 position, Quaternion rotation)
        {
            Matrix4x4 parentMatrix = Matrix4x4.TRS(position, rotation, Vector3.one);
            for (int i = 0; i < previews.Length; i++)
            {
                Matrix4x4 localMatrix = savedRooms[i].transform;
                Matrix4x4 worldMatrix = parentMatrix * localMatrix;

                previews[i].DrawPreview(worldMatrix);
            }
        }

        public void PlaceLevel(Vector3 position, Quaternion rotation)
        {
            GameObject[] undoObjects = new GameObject[savedRooms.Length];
            Matrix4x4 parentMatrix = Matrix4x4.TRS(position, rotation, Vector3.one);

            Undo.SetCurrentGroupName(name + " placed");
            int group = Undo.GetCurrentGroup();

            for (int i = 0; i < savedRooms.Length;i++)
            {
                Matrix4x4 localMatrix = savedRooms[i].transform;
                Matrix4x4 worldMatrix = parentMatrix * localMatrix;
                GameObject spawned = (GameObject)PrefabUtility.InstantiatePrefab(savedRooms[i].prefab);
                spawned.transform.SetPositionAndRotation(worldMatrix.GetPosition(), worldMatrix.rotation);

                Undo.RegisterCreatedObjectUndo(spawned, name + " placed");
            }
            Undo.CollapseUndoOperations(group);
        }

        [System.Serializable]
        public struct SavedRoom
        {
            public GameObject prefab;
            public Matrix4x4 transform;

            public SavedRoom(GameObject prefab, Matrix4x4 transform)
            {
                this.prefab = prefab;
                this.transform = transform;
            }
        }

        
    }
}