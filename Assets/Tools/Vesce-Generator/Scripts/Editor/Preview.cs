﻿using UnityEditor;
using UnityEngine;

namespace VesceGenerator
{
    public class Preview
    {
        private Material material;
        private MeshAndMatrix[] meshes;


        public Preview(Material material, GameObject gameObject)
        {
            if(gameObject != null)
                SetMesh(gameObject);

            if(material != null)
                SetMaterial(material);
        }
        public void SetMesh(GameObject gameObject)
        {
            MeshFilter[] filters = gameObject.GetComponentsInChildren<MeshFilter>();

            meshes = new MeshAndMatrix[filters.Length];
            
            for (int i = 0; i < filters.Length; i++)
            {
                meshes[i].mesh = filters[i].sharedMesh;
                meshes[i].localToWorldMatrix = filters[i].transform.localToWorldMatrix;
            }
        }
    
        public void SetMaterial(Material material)
        {
            this.material = material;
        }

        public void DrawPreview(Matrix4x4 matrix)
        {
            if (material == null || meshes == null || meshes.Length == 0) return;

            material.SetPass(0);
            foreach (var mesh in meshes)
            {
                Matrix4x4 childToWorldMatrix = matrix * mesh.localToWorldMatrix;
                Graphics.DrawMeshNow(mesh.mesh, childToWorldMatrix);
            }
        }

        private struct MeshAndMatrix
        {
            public Mesh mesh;
            public Matrix4x4 localToWorldMatrix;
        }
    }
}