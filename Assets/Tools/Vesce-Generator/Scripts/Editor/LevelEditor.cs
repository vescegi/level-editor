﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace VesceGenerator
{
    public class LevelEditor : EditorWindow
    {
        [MenuItem("Tools/LevelEditor")]
        public static void OpenWindow() => EditorWindow.GetWindow<LevelEditor>();

        [SerializeField] private float findRadius = 5;

        private List<Room> prefabs;
        private Room selectedRoom;
        private Quaternion prefabRotation;

        private SerializedObject serializedObject;
        private SerializedProperty findRadiusP;

        private Preview preview;
        private Material previewMaterial;

        private bool placeLevel;
        private Level levelToPlace;


        private void OnEnable()
        {
            SceneView.duringSceneGui += DuringSceneGUI;

            serializedObject = new SerializedObject(this);
            findRadiusP = serializedObject.FindProperty(nameof(findRadius));


            preview = new Preview(previewMaterial, null);

            prefabRotation = Quaternion.identity;

            if (previewMaterial == null)
            {
                previewMaterial = GetDefaultMaterial();
            }

            preview.SetMaterial(previewMaterial);
        }

        private void OnDisable()
        {
            SceneView.duringSceneGui -= DuringSceneGUI;
        }

        private void OnGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(findRadiusP);
            findRadius = findRadiusP.floatValue.AtLeast(1);

            EditorGUILayout.LabelField("Object placing", EditorStyles.boldLabel);
            if (GUILayout.Button("Load Prefabs"))
            {
                LoadPrefab();
            }

            if (GUILayout.Button("Unload prefab"))
            {
                UnloadPrefab();
            }

            EditorGUILayout.LabelField("Level load", EditorStyles.boldLabel);
            if (GUILayout.Button("Save selected rooms"))
            {
                SaveSelectedRooms();
            }

            if (GUILayout.Button("Load rooms"))
            {
                LoadRooms();
            }

            if (serializedObject.ApplyModifiedProperties())
            {
                SceneView.RepaintAll();
            }

            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                GUI.FocusControl(null);
                Repaint();
            }
        }

        private void DuringSceneGUI(SceneView sceneView)
        {
            DrawGUI();
            RotatePrefab();
            if (selectedRoom == null || previewMaterial == null) return;

            Vector3 mousePosition = GetMousePointingPosition();

            if (!placeLevel)
            {
                PlaceObject(mousePosition);
            }
            else
            {
                PlaceLevel(mousePosition);
            }

            if (Event.current.type == EventType.MouseMove)
            {
                sceneView.Repaint();
            }
        }

        #region Object Placer
        private void DrawGUI()
        {
            Handles.BeginGUI();

            if (prefabs == null || prefabs.Count == 0) return;

            Rect buttonRect = new Rect(8, 8, 50, 50);
            GUIStyle textStyle = new GUIStyle();
            textStyle.normal.textColor = Color.white;
            textStyle.fontStyle = FontStyle.Bold;

            foreach (var prefab in prefabs)
            {
                Texture icon = AssetPreview.GetAssetPreview(prefab.gameObject);

                bool buttonClick = GUI.Button(buttonRect, icon);

                Rect labelRect = CreateOffsetRect(in buttonRect, 20, 150);

                GUI.Label(labelRect, prefab.name, textStyle);

                if (buttonClick)
                {
                    selectedRoom = prefab;
                    preview.SetMesh(prefab.gameObject);
                }

                buttonRect.y += buttonRect.height + 2;
            }

            buttonRect.width += 20;
            buttonRect.height -= 30;

            bool deselectClick = GUI.Button(buttonRect, "Deselect");
            if (deselectClick)
            {
                selectedRoom = null;
            }

            Handles.EndGUI();
        }

        private void PlaceObject(Vector3 mousePosition)
        {
            Vector3 objectPosition = GetObjectPosition(mousePosition);

            Handles.DrawWireDisc(mousePosition, Vector3.up, findRadius);
            preview.DrawPreview(Matrix4x4.TRS(objectPosition, prefabRotation, Vector3.one));

            if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyUp)
            {
                SpawnObject(selectedRoom.gameObject, objectPosition, prefabRotation);
            }

            HandleShortcuts();
        }

        private void HandleShortcuts()
        {
            // ESC: Deselect
            if (Event.current.keyCode == KeyCode.Escape && Event.current.type == EventType.KeyUp)
            {
                selectedRoom = null;
            }

            bool isHoldingCTRL = (Event.current.modifiers & EventModifiers.Control) != 0;
            if (Event.current.type == EventType.ScrollWheel && isHoldingCTRL == true)
            {
                float scrollDirection = Mathf.Sign(Event.current.delta.y);

                serializedObject.Update();
                findRadiusP.floatValue *= 1f - scrollDirection * 0.05f;


                if (serializedObject.ApplyModifiedProperties())
                {
                    Repaint();
                }

                Event.current.Use();
            }
        }

        private static Rect CreateOffsetRect(in Rect buttonRect, float labelHeight, float labelWidth)
        {
            return new Rect(
                buttonRect.x + buttonRect.height + 5,
                buttonRect.y + buttonRect.height * 0.5f - labelHeight * 0.5f,
                labelWidth,
                labelHeight);
        }
        private void RotatePrefab()
        {
            bool isHoldingCTRL = (Event.current.modifiers & EventModifiers.Control) != 0;
            bool isKeyDown = Event.current.type == EventType.KeyDown;
            if (!isHoldingCTRL || !isKeyDown) return;

            KeyCode keyCode = Event.current.keyCode;

            if (keyCode == KeyCode.Q)
            {
                prefabRotation *= Quaternion.AngleAxis(-90, Vector3.up);
            }
            if (keyCode == KeyCode.E)
            {
                prefabRotation *= Quaternion.AngleAxis(90, Vector3.up);
            }
        }

        /// <summary>
        /// Cast the ray to the zero plane and return the point it hits
        /// </summary>
        /// <returns></returns>
        private Vector3 GetMousePointingPosition()
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            Plane plane = new Plane(Vector3.up, Vector3.zero);

            if (plane.Raycast(ray, out float distance))
                return ray.GetPoint(distance);

            return Vector3.zero;
        }

        private void SpawnObject(GameObject gameObject, Vector3 position, Quaternion rotation)
        {
            GameObject spawned = (GameObject)PrefabUtility.InstantiatePrefab(gameObject);
            spawned.transform.SetPositionAndRotation(position, rotation);
            Undo.RegisterCreatedObjectUndo(spawned, $"{spawned.name} spawned");
        }

        private Vector3 GetObjectPosition(Vector3 startingPoint)
        {
            Vector3 snappedPoint;
            if (TryToSnapObject(startingPoint, out snappedPoint))
            {
                // Check if the spot is free
                if (!Physics.CheckSphere(startingPoint, 1f))
                {
                    return snappedPoint;
                }
            }

            return startingPoint;
        }

        private bool TryToSnapObject(Vector3 point, out Vector3 snappedPoint)
        {
            snappedPoint = point;
            Room closestRoom = GetClosestRoomInPoint(point);

            if (closestRoom == null) return false;

            // Get now the direction i want
            if (closestRoom.TryGetPoint(point, out Vector3 roomPoint, out Direction direction))
            {
                if (selectedRoom.IsDirectionValid(direction, prefabRotation))
                {
                    //Debug.Log(direction);
                    snappedPoint = roomPoint;
                    return true;
                }
            }
            return false;
        }

        private Room GetClosestRoomInPoint(Vector3 mousePoint)
        {
            Vector3 point0 = new Vector3(mousePoint.x, mousePoint.y - 5, mousePoint.z);
            Vector3 point1 = new Vector3(mousePoint.x, mousePoint.y + 5, mousePoint.z);

            Collider[] colliders = Physics.OverlapCapsule(point0, point1, findRadius);

            if (colliders == null || colliders.Length == 0) return null;

            Collider closest = null;
            float closestDistance = float.MaxValue;
            foreach (Collider collider in colliders)
            {
                float distance = Vector3.SqrMagnitude(collider.transform.position - mousePoint);

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closest = collider;
                }
            }

            if (closest == null || closest.transform.parent == null) return null;

            if (closest.transform.parent.TryGetComponent(out Room room))
            {
                return room;
            }

            return null;
        }
        #endregion

        #region Loading/Unloading assets

        private static string AbsoluteToRelativePath(string absPath)
        {
            string projectFolderPath = Application.dataPath;
            if (!absPath.Contains(projectFolderPath))
            {
                Debug.LogError("You need to select a folder inside the project's asset folder");
                return "";
            }

            return absPath.Substring(absPath.IndexOf("Assets/"));
        }

        private void LoadPrefab()
        {
            string absPath = EditorUtility.OpenFolderPanel("Select the prefab's folder", "Assets/", "");
            if (absPath == "") return;

            string relativePath = AbsoluteToRelativePath(absPath);

            prefabs = GetPrefabsFromPath(relativePath);

            Debug.Log($"Loaded {prefabs?.Count} prefabs in {relativePath}");
        }

        private void UnloadPrefab()
        {
            selectedRoom = null;
            prefabs = null;

            SceneView.RepaintAll();
        }

        private List<Room> GetPrefabsFromPath(string path)
        {
            List<Room> rooms = new List<Room>();
            string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { path });

            if (guids.Length == 0)
            {
                return new List<Room>();
            }

            IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
            GameObject[] prefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();
            foreach (GameObject prefab in prefabs)
            {
                if (prefab.TryGetComponent(out Room room))
                {
                    rooms.Add(room);
                }
            }

            return rooms;
        }

        private Material GetDefaultMaterial()
        {
            Material material = new Material(Shader.Find("VesceTools/PreviewShader"));

            material.SetColor("_Color", new Color(0f, 0.8863635f, 1f, 1f));
            material.SetFloat("_Alpha", 0.564f);
            material.SetFloat("_AmbientLight", 0.2f);

            Debug.Log(material);
            return material;
        }

        #endregion

        #region Save/Load level
        private void SaveSelectedRooms()
        {
            List<Room> roomsToSave = new();

            foreach (var gameObject in Selection.gameObjects)
            {
                if (gameObject.TryGetComponent(out Room room))
                {
                    roomsToSave.Add(room);
                }
            }

            if (roomsToSave.Count <= 0)
            {
                Debug.LogWarning("No rooms selected!");
                return;
            }

            string path = EditorUtility.SaveFilePanelInProject("Save this level", "level", "asset",
                "Please enter a file name to save the level to");

            if (path.Length != 0)
            {
                Level level = ScriptableObject.CreateInstance<Level>();
                level.SaveData(roomsToSave);

                AssetDatabase.CreateAsset(level, path);
                AssetDatabase.SaveAssets();
            }
        }

        private void LoadRooms()
        {
            string absPath = EditorUtility.OpenFilePanel("Select the levels to load", "Assets", "asset");

            if (absPath.Length == 0)
            {
                return;
            }

            string relativePath = AbsoluteToRelativePath(absPath);
            if (relativePath.Length == 0)
            {
                return;
            }

            levelToPlace = AssetDatabase.LoadAssetAtPath<Level>(relativePath);
            levelToPlace.GeneratePreviews(previewMaterial);
            placeLevel = true;
        }

        private void PlaceLevel(Vector3 position)
        {
            levelToPlace.DrawPreview(position, prefabRotation);
            
            if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyUp)
            {
                levelToPlace.PlaceLevel(position, prefabRotation);
                placeLevel = false;
            }

            if (Event.current.keyCode == KeyCode.Escape && Event.current.type == EventType.KeyUp)
            {
                placeLevel = false;
                levelToPlace = null;
            }
        }
        #endregion
    }
}