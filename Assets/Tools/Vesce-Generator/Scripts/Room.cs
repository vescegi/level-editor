﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace VesceGenerator
{
    [ExecuteAlways]
    public class Room : MonoBehaviour
    {
        [SerializeField] private Direction[] openDirections;

        public bool TryGetPoint(Vector3 roomPosition, out Vector3 point, out Direction worldDirectionAttached)
        {
            Vector3 directionFromPoint = (roomPosition - transform.position).normalized;
            int angle = ((int)(Mathf.Atan2(directionFromPoint.x, directionFromPoint.z) * Mathf.Rad2Deg)).To360Angle().ToClosestInt(90);
            if (angle == 360) angle = 0;


            Direction worldDirection = (Direction)angle;
            Direction localDirection = WorldToLocalDirection(worldDirection, transform.rotation);
            
            foreach (var direction in openDirections)
            {
                if (direction == localDirection)
                {
                    point =
                        transform.position +
                        (Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward) * 2;

                    worldDirectionAttached = (Direction)(angle + 180).To360Angle();
                    return true;
                }
            }
            worldDirectionAttached = Direction.North;
            point = Vector3.zero;
            return false;
        }

        public bool IsDirectionValid(Direction direction, Quaternion rotation)
        {
            Direction localDirection = Room.WorldToLocalDirection(direction, rotation);

            foreach(var openDirection in openDirections)
            {
                if(localDirection == openDirection) return true;
            }

            return false;
        }


        [ContextMenu("Print world directions")] 
        private void PrintWorldDirections()
        {
            StringBuilder sb = new StringBuilder();
            foreach(var direction in openDirections)
            {
                sb.Append((Direction)WorldToLocalDirection(direction, transform.rotation));
                sb.Append(" ");
            }

            Debug.Log(sb);
        }

        #region Math
        public static Direction LocalToWorldDirection(Direction direction, Quaternion rotation)
        {
            int angle = ((int)direction - QuaternionToAngle(rotation)).To360Angle();

            return (Direction)angle;
        }

        public static Direction WorldToLocalDirection(Direction direction, Quaternion rotation)
        {
            int angle = (QuaternionToAngle(rotation) - (int)direction).To360Angle();
            
            return (Direction)angle;
        }

        private static int QuaternionToAngle(Quaternion rotation)
        {
            int angle = ((int)rotation.eulerAngles.y).To360Angle().ToClosestInt(90);
            if (angle == 360) angle = 0;
            return angle;
        }
        #endregion
    }

    public enum Direction
    {
        North = 0,
        West = 90,
        South = 180,
        East = 270
    }
}
